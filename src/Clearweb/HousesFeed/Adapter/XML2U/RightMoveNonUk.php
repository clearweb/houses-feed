<?php namespace Clearweb\HousesFeed\Adapter\XML2U;

use Clearweb\HousesFeed\IFeedAdapter;
use Clearweb\HousesFeed\Fetcher\IUrlFetcher;
use Clearweb\HousesFeed\Fetcher\UrlFetcher;

class RightMoveNonUk implements IFeedAdapter
{
    private $urlFetcher = null;
	
    const STATE_INIT       = 0;
    const STATE_HEADER     = 1;
    const STATE_DEFINITION = 2;
    const STATE_DATA       = 3;
    const STATE_END        = 4;
    
	public function __construct()
	{
		$this->setUrlFetcher(new UrlFetcher);
	}
	
	public function setUrlFetcher(IUrlFetcher $fetcher)
	{
		$this->urlFetcher = $fetcher;
	}
	
	public function getUrlFetcher()
	{
		return $this->urlFetcher;
	}
    
	public function getName()
	{
		return 'xml2u-right-move';
	}
    
    public function getHousesFromUrl($url)
	{
		$houses = array();
		
		$content = $this->getUrlFetcher()->fetch($url);
        
        $data = $this->readMHT($content);
        
        foreach($data as $property) {
            $house = array(
                           'id' => $property['AGENT_REF'],
                           'title' => '',
                           'price' => $property['PRICE'],
                           'type' => $this->translateType($property['PROP_SUB_ID']),
                           'build_type' => 'Unknown',
                           'nr_rooms' => $property['BEDROOMS'],
                           'bath_rooms' => 0,
                           'surface' => 0,
                           'living_surface' => 0,
                           'volume' => 0,
                           'description' => $property['DESCRIPTION'],
                           'postal_code' => $property['ZIPCODE'],
                           'street' => $property['STREET_NAME'],
                           'house_number' => $property['HOUSE_NAME_NUMBER'],
                           'region' => $property['OS_REGION'],
                           'country' => $property['COUNTRY_CODE'],
                           'city' => $property['OS_TOWN_CITY'],
                           'attributes' => array(),
                           'images' => array(),
                           );
                
            // attributes
            for($i=1; $i<=10; $i++) {
                $featureName = 'FEATURE'.$i;
                if ( ! empty($property[$featureName])) {
                    $house['attributes'][] = strtolower(str_replace(' ', '-', $property[$featureName]));
                }
            }
            
            // images
            for($i=1; $i<=19; $i++) {
                $imageUrl  = 'MEDIA_IMAGE_'.$i;
                $imageText = 'MEDIA_IMAGE_TEXT_'.$i;
                if ( ! empty($property[$imageUrl])) {
                    $house['images'][] = array(
                                               'url' => $property[$imageUrl],
                                               'title' => $property[$imageText]
                                               )
                        ;
                }
            }
            
            $houses[] = $house;
        }
        
        return $houses;
    }
    
    private function translateType($typeId) {
        $types = array(
                       0=>"Not Specified",
                       1=>"Terraced",
                       2=>"End of Terrace",
                       3=>"Semi-Detached",
                       4=>"Detached",
                       5=>"Mews",
                       6=>"Cluster House",
                       7=>"Ground Flat",
                       8=>"Flat",
                       9=>"Studio",
                       10=>"Ground Maisonette",
                       11=>"Maisonette",
                       12=>"Bungalow",
                       13=>"Terraced Bungalow",
                       14=>"Semi-Detached Bungalow",
                       15=>"Detached Bungalow",
                       16=>"Mobile Home",
                       17=>"Hotel",
                       18=>"Guest House",
                       19=>"Commercial Property",
                       20=>"Land",
                       21=>"Link Detached House",
                       22=>"Town House",
                       23=>"Cottage",
                       24=>"Chalet",
                       27=>"Villa",
                       28=>"Apartment",
                       29=>"Penthouse",
                       30=>"Finca",
                       43=>"Barn Conversion",
                       44=>"Serviced Apartments",
                       45=>"Parking",
                       46=>"Sheltered Housing",
                       47=>"Retirement Property",
                       48=>"House Share",
                       49=>"Flat Share",
                       50=>"Park Home",
                       51=>"Garages",
                       52=>"Farm House",
                       53=>"Equestrian",
                       56=>"Duplex",
                       59=>"Triplex",
                       62=>"Longere",
                       65=>"Gite",
                       68=>"Barn",
                       71=>"Trulli",
                       74=>"Mill",
                       77=>"Ruins",
                       80=>"Restaurant",
                       83=>"Cafe",
                       86=>"Mill",
                       89=>"Trulli",
                       92=>"Castle",
                       95=>"Village House",
                       101=>"Cave House",
                       104=>"Cortijo",
                       107=>"Farm Land",
                       110=>"Plot",
                       113=>"Country House",
                       116=>"Stone House",
                       117=>"Caravan",
                       118=>"Lodge",
                       119=>"Log Cabin",
                       120=>"Manor House",
                       121=>"Stately Home",
                       125=>"Off-Plan",
                       128=>"Semi-detached Villa",
                       131=>"Detached Villa",
                       134=>"Bar",
                       137=>"Shop",
                       140=>"Riad",
                       141=>"House Boat",
                       142=>"Hotel Room",
                       );
        
        return $types[$typeId];
    }
    
    protected function readMHT($content) {
        $state = self::STATE_INIT;
        
        $fieldEnd = '^';
        $rowEnd = '~';
        
        $keys = array();
        $values = array();
        
        $content = str_replace("=\r\n", '', $content);
        
        foreach(preg_split("/((\r\n?)|(\r?\n))/", $content) as $line){
            switch($state) {
                case self::STATE_INIT:
                    if (preg_match('/#HEADER#/', $line)) {
                        $state = self::STATE_HEADER;
                    }
                    break;
                case self::STATE_HEADER:
                    if (preg_match('/#DEFINITION#/', $line)) {
                        $state = self::STATE_DEFINITION;
                    } elseif(preg_match('/EOF : \'(.+)\'/', $line, $matches)) {
                        $fieldEnd = $matches[1];
                    } elseif(preg_match('/EOR : \'(.+)\'/', $line, $matches)) {
                        $rowEnd = $matches[1];
                    }
                    break;
                case self::STATE_DEFINITION:
                    if (preg_match('/^#DATA#$/', $line)) {
                        $state = self::STATE_DATA;
                    } elseif(preg_match('/^.+'.$rowEnd.'$/', $line)) {
                        $keys = explode($fieldEnd, $line);
                    }
                    break;
                case self::STATE_DATA:
                    if (preg_match('/#END#/', $line)) {
                        $state = self::STATE_END;
                    } elseif(preg_match('/^.*'.$rowEnd.'$/', $line)) {
                        $values = explode($fieldEnd, $line);
                        $dataRow = array_combine($keys, $values);
                        $data[] = $dataRow;
                    }
                    break;
                case self::STATE_END:
                    return $data;
                    break;
            }
        }
        
        return $data;
    }
}