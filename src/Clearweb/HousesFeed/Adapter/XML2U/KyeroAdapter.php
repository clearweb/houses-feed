<?php namespace Clearweb\HousesFeed\Adapter\XML2U;

use Clearweb\HousesFeed\IFeedAdapter;
use Clearweb\HousesFeed\Fetcher\IUrlFetcher;
use Clearweb\HousesFeed\Fetcher\UrlFetcher;

use SimpleXMLElement;

class KyeroAdapter implements IFeedAdapter
{
    private $urlFetcher = null;
    
	public function __construct()
	{
		$this->setUrlFetcher(new UrlFetcher);
	}
	
	public function setUrlFetcher(IUrlFetcher $fetcher)
	{
		$this->urlFetcher = $fetcher;
	}
	
	public function getUrlFetcher()
	{
		return $this->urlFetcher;
	}
    
	public function getName()
	{
		return 'xml2u-kyero';
	}
    
    public function getHousesFromUrl($url)
	{
		$houses = array();
		
		$content = $this->getUrlFetcher()->fetch($url);
        
		$feed = new SimpleXMLElement($content);
        
        foreach($feed->property as $property) {
            $geoData = $this->getGoogleGeolocationData($property);
            
            $house = array(
                           'id' => (string) $property->id,
                           'title' => '',
                           'price' => (string) $property->price,
                           'type' => $this->translateType($property),
                           'build_type' => $this->getBuildType($property),
                           'nr_rooms' => (int) $property->beds,
                           'bath_rooms' => (int) $property->baths,
                           'surface' => (string) $property->surface_area->plot,
                           'living_surface' => (string) $property->surface_area->built,
                           'volume' => 0,
                           'description' => $this->getDescription($property),
                           'postal_code' => $geoData['postal_code'],
                           'street' => $geoData['street'],
                           'house_number' => $geoData['house_number'],
                           'region' => $geoData['region'],
                           'country' => $geoData['country'],
                           'city' => $geoData['city'],
                           'attributes' => array(),
                           'images' => array(),
                           );
            
            // attributes
            if (isset($property->features)) {
                foreach($property->features->feature as $feature) {
                    if ( ! (
                            empty($feature) || preg_match('#(built|plot) size [0-9]+ m2#', $feature)
                            )
                         ) {
                        $house['attributes'][] = strtolower(str_replace(' ', '-', (string)$feature));
                    }
                }
            }
            
            // images
            foreach($property->images->image as $image) {
                $house['images'][] = array('url' => (string) $image->url);
            }
            
            $houses[] = $house;
        }
        
        return $houses;
    }
    
	public function translateType($property)
	{
        if (isset($property->type->nl)) {
            $type = (string) $property->type->nl;
        } elseif(isset($property->type->en)) {
            $type = (string) $property->type->en;
        } else {
            $type = (string) $property->type;
        }
        
		return ucfirst(strtolower($type));
	}
    
    public function getDescription($property) {
        if (isset($property->desc->nl)) {
            $description = (string) $property->desc->nl;
        } elseif (isset($property->desc->en)) {
            $description = (string) $property->desc->en;
        } else {
            $description = (string) $property->desc;
        }
        
        return $description;
    }
    
    public function getBuildType($property)
    {
        $type = 'old';
        
        if ( ! empty($property->new_build)) {
            $type = 'new';
        }
        
        return $type;
    }

    protected function getGoogleGeolocationData($property) {
        $city = (string) $property->town;
        $region = (string) $property->province;
        $country = 'ES';
        $postal_code = (string) $property->postcode;
        $street = '';
        $house_number = '';
        
        
        $location = $property->location;
        if ( ! (empty($location->latitude) || empty($location->longitude))) {
                $url  = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".urlencode("{$location->latitude},{$location->longitude}");
        } else {
                $url  = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode("{$city}, {$region}, Spain");
        }
        
        $json = $this->getUrlFetcher()->fetch($url);
        
        $data = json_decode($json, true);
        
        if (isset($data['results'][0]['address_components'])) {
            foreach($data['results'][0]['address_components'] as $component) {
                if (empty($city) && in_array('administrative_area_level_2', $component['types'])) {
                    $city = $this->toASCII($component['long_name']);
                } elseif (empty($region) && in_array('administrative_area_level_1', $component['types'])) {
                    $region = $this->toASCII($component['long_name']);
                } elseif (in_array('postal_code', $component['types'])) {
                    $postal_code = $component['long_name'];
                } elseif (in_array('route', $component['types'])) {
                    $street = $component['long_name'];
                } elseif (in_array('street_number', $component['types'])) {
                    $house_number = $component['long_name'];
                }
            }
        }
        
        return compact('country', 'region', 'city', 'postal_code', 'street', 'house_number');
    }
    
    private function toASCII( $str )
    {
        return strtr(utf8_decode($str), 
                     utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
                     'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
    }
    
}