<?php namespace Clearweb\HousesFeed\Adapter\XML2U;

use Clearweb\HousesFeed\IFeedAdapter;
use Clearweb\HousesFeed\Fetcher\IUrlFetcher;
use Clearweb\HousesFeed\Fetcher\UrlFetcher;

use SimpleXMLElement;

class DefaultAdapter implements IFeedAdapter
{
    private $urlFetcher = null;
	
	public function __construct()
	{
		$this->setUrlFetcher(new UrlFetcher);
	}
	
	public function setUrlFetcher(IUrlFetcher $fetcher)
	{
		$this->urlFetcher = $fetcher;
	}
	
	public function getUrlFetcher()
	{
		return $this->urlFetcher;
	}
    
	public function getName()
	{
		return 'xml2u-default';
	}
	
	public function getHousesFromUrl($url)
	{
		$houses = array();
		
		$content = $this->getUrlFetcher()->fetch($url);
        
		$feed = new SimpleXMLElement($content);
        
        foreach($feed->Clients->Client as $client) {
            foreach($client->properties->Property as $property) {
                $geoData = $this->getGoogleGeolocationData($property);
                
                $descRaw = (string) $property->Description->description;
                if (preg_match('#<nl>(.+)</nl>#', $descRaw, $matches)) {
                    $description = $matches[1];
                } else {
                    $description = (string) $property->Description->description;
                }
                
                $house = array(
                               'id' => (string) $property->propertyid,
                               'title' => (string) $property->Description->title,
                               'price' => (string) $property->Price->price,
                               'type' => $this->translateType((string) $property->Description->propertyType),
                               'build_type' => $this->getBuildType($property),
                               'nr_rooms' => (string) $property->Description->bedrooms,
                               'bath_rooms' => (string) $property->Description->fullBathrooms,
                               'surface' => (string) $property->Description->PlotSize->plotSize,
                               'living_surface' => (string) $property->Description->FloorSize->floorSize,
                               'volume' => 0,
                               'description' => $description,
                               'postal_code' => $geoData['postal_code'],
                               'street' => $geoData['street'],
                               'house_number' => $geoData['house_number'],
                               'region' => $geoData['region'],
                               'country' => $geoData['country'],
                               'city' => $geoData['city'],
                               'attributes' => array(),
                               'images' => array(),
                               );
                
                // attributes
                if ($property->Description->heating == 'Yes') {
                    $house['attributes'][] = 'heating';
                }
                
                if ($property->Description->elevator == 'Yes') {
                    $house['attributes'][] = 'elevator';
                }
                
                if ($property->Description->fittedKitchen == 'Yes') {
                    $house['attributes'][] = 'fitted-kitchen';
                }
                
                if ($property->Description->assistedLiving == 'Yes') {
                    $house['attributes'][] = 'assisted-living';
                }
                
                if ($property->Description->wheelchairFriendly == 'Yes') {
                    $house['attributes'][] = 'wheelchair-friendly';
                }
                
                if ($property->Description->balcony == 'Yes') {
                    $house['attributes'][] = 'balcony';
                }
                
                if ($property->Description->terrace == 'Yes') {
                    $house['attributes'][] = 'terrace';
                }

                if ($property->Description->swimmingPool == 'Yes') {
                    $house['attributes'][] = 'swimming-pool';
                }
                
                for($i=1; $i<=10; $i++) {
                    $featureName = 'Feature'.$i;
                    if ( ! empty($property->Description->Features->$featureName)) {
                        $house['attributes'][] = strtolower(str_replace(' ', '-', $property->Description->Features->$featureName));
                    }
                }
                
                // images
                foreach($property->images->image as $image) {
                    $house['images'][] = array('url' => (string) $image->image, 'title' => (string) $image->alttext);
                }
                
                $houses[] = $house;
            }
		}
        
        return $houses;
	}
    
	public function translateType($type)
	{
		return ucfirst(strtolower($type));
	}
    
    public function getBuildType($property)
    {
        $type = 'old';
        
        if (strtolower($property->Description->newBuild) == 'new') {
            $type = 'new';
        } elseif ($property->Description->yearBuilt > 2005 ) {
            $type = 'new';
        }
        
        return $type;
    }
    
    protected function getGoogleGlobalGeolocationData($rawRegion, $rawLocation) {
        $url  = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode("$rawLocation, $rawRegion");
        
        $json = $this->getUrlFetcher()->fetch($url);
        
        $data = json_decode($json, true);
        
        $city   = '';
        $region = '';
        
        if (isset($data['results'][0]['address_components'])) {
            foreach($data['results'][0]['address_components'] as $component) {
                if (in_array('administrative_area_level_2', $component['types'])) {
                    $city = $this->toASCII($component['long_name']);
                } elseif (in_array('administrative_area_level_1', $component['types'])) {
                    $region = $this->toASCII($component['long_name']);
                }
            }
        }
        
        if (empty($city)) {
            $city = $rawLocation;
        }
        
        if (empty($region)) {
            $region = $rawRegion;
        }
        
        return compact('city', 'region');
    }
    
    protected function getGoogleGeolocationData($property) {
        $geoData = $this->getGoogleGlobalGeolocationData($property->Address->region, $property->Address->location);
        extract($geoData);

        $country = (string) $property->Address->{'countryCodeISO3166-1-alpha2'};
        if (empty($country)) {
            $country = (string) $property->Address->country;
        }
        
        $postal_code = (string) $property->Address->postcode;
        $street = (string) $property->Address->street;
        $house_number = (string) $property->Address->house_number;
        
        $latitude  = (string) $property->Address->latitude;
        $longitude = (string) $property->Address->longitude;
        
        if ( ! (empty($latitude) || empty($longitude))) {
            $url  = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".urlencode("{$latitude},{$longitude}");
            
            $json = $this->getUrlFetcher()->fetch($url);
        
            $data = json_decode($json, true);
        
            if (isset($data['results'][0]['address_components'])) {
                foreach($data['results'][0]['address_components'] as $component) {
                    if (empty($city) && in_array('administrative_area_level_2', $component['types'])) {
                        $city = $this->toASCII($component['long_name']);
                    } elseif (empty($region) && in_array('administrative_area_level_1', $component['types'])) {
                        $region = $this->toASCII($component['long_name']);
                    } elseif (empty($postal_code) && in_array('postal_code', $component['types'])) {
                        $postal_code = $component['long_name'];
                    } elseif (empty($street) && in_array('route', $component['types'])) {
                        $street = $component['long_name'];
                    } elseif (empty($house_number) && in_array('street_number', $component['types'])) {
                        $house_number = $component['long_name'];
                    }
                }
            }
        }
            
        return compact('country', 'region', 'city', 'postal_code', 'street', 'house_number');
    }
    
    private function toASCII( $str )
    {
        return strtr(utf8_decode($str), 
                     utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
                     'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
    }
}