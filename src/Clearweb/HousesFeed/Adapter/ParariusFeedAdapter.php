<?php namespace Clearweb\HousesFeed\Adapter;

use Clearweb\HousesFeed\Fetcher\IUrlFetcher;
use Clearweb\HousesFeed\Fetcher\UrlFetcher;
use Clearweb\HousesFeed\IFeedAdapter;

use SimpleXMLElement;

class ParariusFeedAdapter implements IFeedAdapter
{	
	private $urlFetcher = null;
	
	public function __construct()
	{
		$this->setUrlFetcher(new UrlFetcher);
	}
	
	public function setUrlFetcher(IUrlFetcher $fetcher)
	{
		$this->urlFetcher = $fetcher;
	}
	
	public function getUrlFetcher()
	{
		return $this->urlFetcher;
	}

	public function getName()
	{
		return 'pararius';
	}
	
	public function getHousesFromUrl($url)
	{
		$houses = array();
		
		$content = $this->getUrlFetcher()->fetch($url);
		
		$feed = new SimpleXMLElement($content);
		
		foreach($feed->woning as $woning) {
			$house = array(
                           'id' => $woning['identificatie'],
                           'title' => '',
						   'price' => $this->getPrice($woning),
						   'type' => $this->translateType((string) $woning->type),
						   'build_type' => 'Unknown',
						   'nr_rooms' => (string) $woning->aantalKamers,
						   'surface' => (string) $woning->perceelOppervlak,
						   'living_surface' => (string) $woning->woonOppervlak,
						   'volume' => (string) $woning->inhoud,
						   'description' => (string) $woning->beschrijving,
						   'postal_code' => (string) $woning->postcode,
						   'street' => trim((string) $woning->adres['straatnaam']),
						   'house_number' => '0',
						   'country' => 'NL',
						   'city' => trim(ucfirst(strtolower($woning->plaats))),
						   'attributes' => array()
						   );
			
			// attributes
			
			if ( ! empty($woning->gemeubileerd) && $woning->gemeubileerd != 'nee') {
				$house['attributes'][] = ucfirst(strtolower($woning->gemeubileerd));
			}
			
			if ($woning->garage == 'ja') {
				$house['attributes'][] = 'Garage';
			}
			
			if($woning->tuin == 'ja') {
				$house['attributes'][] = 'Garden';
			}
			
			if($woning->balkon == 'ja') {
				$house['attributes'][] = 'Balcony';
			}
			
			// images
			
			$house['images'] = array();
			
			foreach($woning->afbeeldingen->afbeeldingURL as $imageUrl) {
				$house['images'][] = array('url' => (string) $imageUrl);
			}
			
			$houses[] = $house;
		}
		
		return $houses;
	}
	
	private function getPrice($woning)
	{
		if ((string) $woning->prijsType == 'exclusief')
			$price = (string) $woning->prijs;
		else
			$price = (string) $woning->prijs / 1.21;
		
		return $price;
	}
	
	public function translateType($type)
	{
		return ucfirst(strtolower($type));
	}
}