<?php namespace Clearweb\HousesFeed;

interface ILogWriter
{
    public function setFeed(IFeed $feed);
    public function write($message);
    public function flush();
}