<?php namespace Clearweb\HousesFeed\Fetcher;

class ImageFetcher implements IImageFetcher
{
	public function fetch($url, $dir)
	{
		$path = tempnam($dir, 'house-img-');
		
		chmod($path, 0777);
		
		file_put_contents($path, file_get_contents($url));
		
		return $path;
	}
}