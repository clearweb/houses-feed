<?php namespace Clearweb\HousesFeed\Fetcher;

interface IUrlFetcher
{
	/**
	 * Fetches the content of the url
	 */
	public function fetch($url);
}