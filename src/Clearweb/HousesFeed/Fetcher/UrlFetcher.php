<?php namespace Clearweb\HousesFeed\Fetcher;

class UrlFetcher implements IUrlFetcher
{
	public function fetch($url)
	{
		return file_get_contents($url);
	}
}