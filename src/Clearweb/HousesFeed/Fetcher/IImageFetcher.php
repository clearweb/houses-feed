<?php namespace Clearweb\HousesFeed\Fetcher;

interface IImageFetcher
{
	/**
	 * Fetches an image and return its path
	 * @param string $reference the reference of the image
	 * @param string $dir the path of the dir to save the image to
	 * @return string path of the fetched image
	 */
	public function fetch($reference, $dir);
}
