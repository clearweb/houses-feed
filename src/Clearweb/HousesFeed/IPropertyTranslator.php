<?php namespace Clearweb\HousesFeed;

interface IPropertyTranslator
{
	/**
	 * Gets the correct value for the house type
	 */
	public function translateType($type);
	
	/**
	 * Gets the correct value for the house type
	 */
	public function translateBuildType($buildType);
	
	/**
	 * Gets the correct value for the attribute
	 */
	public function translateAttribute($attribute);
}