<?php namespace Clearweb\HousesFeed;


class NullLogWriter implements ILogWriter
{
    public function setFeed(IFeed $feed) {}
    public function write($message) {}
    public function flush() {}
}