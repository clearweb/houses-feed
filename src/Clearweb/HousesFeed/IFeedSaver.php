<?php namespace Clearweb\HousesFeed;

use Clearweb\HousesFeed\Fetcher\IImageFetcher;

/**
 * An object knowing how to save data coming from the house feeds
 */
interface IFeedSaver
{	
	/**
	 * Sets the fetcher of the images
	 */
	public function setImageFetcher(IImageFetcher $fetcher);
	
	/**
	 * Gets the fetcher of the images
	 */
	public function getImageFetcher();
	
	/**
	 * Sets the prperty translator
	 */
	public function setPropertyTranslator(IPropertyTranslator $translator);
	
	/**
	 * Gets the prperty translator
	 */
	public function getPropertyTranslator();
	
	/**
	 * Saves the data coming from the house feeds
	 * 
	 * @param array $house an array with the house data. For example: <code>array('title' => 'Mooie woning in Amsterdam', 'street'=>'Solebaystraat', 'postal_code'=>'1055ZK', 'city'=>'Amsterdam', 'country'=>'Nederland')</code>
	 * @param int $userId the id of the user to save the houses in his name.
     * @param \Clearweb\HousesFeed\IFeed the feed we are importing.
	 */
	public function saveHouseData(array $house, $userId, IFeed $feed);
}