<?php namespace Clearweb\HousesFeed;

/**
 * Represents a feed ready to be executed for a user with a defined url
 */
interface IFeed
{
	/**
	 * Sets the url we will query
	 */
	public function setUrl($url);
	
	/**
	 * Gets the url we will query
	 */
	public function getUrl();
	
	/**
	 * Sets the user id the feed will execute for
	 */
	public function setUserId($id);
	
	/**
	 * Gets the user id the feed will execute for
	 */
	public function getUserId();
	
	/**
	 * Sets the feed adapter
	 * @param IFeedAdapter $adapter the adapter
	 */
	public function setFeedAdapter(IFeedAdapter $adapter);
	
	/**
	 * Gets the feed adapter
	 * @return IFeedAdapter $adapter the adapter
	 */
	public function getFeedAdapter();
	
	/**
	 * Queries the feed and gets the houses from it
	 * @pre Url should be set with <code>setUrl</code>
	 * @return array with house data
	 */
	public function getHouses();
}