<?php namespace Clearweb\HousesFeed;

use Clearweb\HousesFeed\Fetcher\IUrlFetcher;

interface IFeedAdapter
{
	/**
	 * The name of the feed adapter
	 * @return string the name of the adapter
	 */
	public function getName();

	/**
	 * Sets the fetcher of the urls
	 */
	public function setUrlFetcher(IUrlFetcher $fetcher);
	
	/**
	 * Gets the fetcher of the urls
	 */
	public function getUrlFetcher();
	
	/**
	 * Calls the url and gets the houses from it
	 * @param string $url the url to get houses from
	 * @return array with house data
	 */
	public function getHousesFromUrl($url);
}