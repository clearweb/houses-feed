<?php namespace Clearweb\HousesFeed;

/**
 * Knows how to execute all feeds registered to it.
 */
class FeedExecuter
{
	protected $feeds     = array();
	protected $providers = array();
    protected $logWriter = null;
	protected $feedSaver = null;
	
    public function __construct()
    {
        $this->setLogWriter(new NullLogWriter);
    }
    
	public function addFeed(IFeed $feed)
	{
		$this->feeds[] = $feed;
		
		return $this;
	}
	
	public function addProvider(IFeedProvider $feedProvider) {
		$this->providers[] = $feedProvider;
		
		return $this;
	}
	
	public function getFeeds()
	{
		$feeds = $this->feeds;
		
		foreach($this->providers as $provider) {
			$feeds = array_merge($feeds, $provider->getFeeds());
		}
		
		return $feeds;
	}
	
	public function setFeedSaver(IFeedSaver $feedSaver)
	{
		$this->feedSaver = $feedSaver;
		
		return $this;
	}
    
    public function getLogWriter()
    {
        return $this->logWriter;
    }
    
    public function setLogWriter(ILogWriter $writer)
    {
        $this->logWriter = $writer;
        
        return $this;
    }
	
	public function execute()
	{
        set_time_limit(0);
        
		foreach($this->getFeeds() as $feed) {
			$this->executeFeed($feed);
		}
	}
    
    protected function executeFeed(IFeed $feed) {
        $this->getLogWriter()->setFeed($feed);
        
        $houses = $feed->getHouses();
        foreach($houses as $house) {
            $this->feedSaver->setLogWriter($this->getLogWriter());
            $this->feedSaver->saveHouseData($house, $feed->getUserId(), $feed);
        }
        
        $this->getLogWriter()->flush();
    }
}