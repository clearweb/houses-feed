<?php namespace Clearweb\HousesFeed;

interface IFeedProvider
{
	/**
	 * Fetches all registered feeds.
	 */
	public function getFeeds();
}