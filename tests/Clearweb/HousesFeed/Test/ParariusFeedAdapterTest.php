<?php namespace Clearweb\HousesFeed\Test;

use Clearweb\HousesFeed\Adapter\ParariusFeedAdapter;

use PHPUnit_Framework_TestCase;

class ParariusFeedAdaperTest extends PHPUnit_Framework_TestCase
{
	/**
	 * Tests the parsing of two houses from a XML-file.
	 */
	public function testParseTwoHouses()
	{
		$pararius = new ParariusFeedAdapter;
		$pararius->setUrlFetcher(new ParariusFakeUrlFetcher);
		
		$houses = $pararius->getHousesFromUrl('http://example.com');
		
		$this->assertCount(2, $houses, "Pararius feed expected to find two houses here...");
		
		
		// test basic properties
		$this->assertEquals($houses[0]['price'], '775');
		$this->assertEquals($houses[0]['surface'], '90');
		$this->assertEquals($houses[0]['nr_rooms'], '2');
		$this->assertEquals($houses[0]['description'], html_entity_decode('&lt;p&gt;Let op! Voor deze woning betaalt u geen bemiddelingscourtage, u betaalt bij ons enkel contract/administratiekosten a € 300, – excl. 21% btw.&lt;/p&gt;&lt;p&gt;Schitterend (nieuwbouw) appartement met 1 slaapkamer aan de Gildenlaan in Apeldoorn. Deze woning is voorzien van een gloednieuwe PVC vloer en nette wand afwerking. Vanuit de woonkamer is er toegang tot een ruim balkon met vanaf het middag uur de zon erop.&lt;/p&gt;&lt;p&gt;Het appartement is gelegen in woonwijk De Maten op winkelcentrum De Eglantier. Het complex is gebouwd onder stijlvolle architectuur en afgewerkt met hoogwaardige materialen. Er is een afgesloten entree en een moderne videofoon. De liftinstallaties zijn ook bereikbaar vanuit de parkeergarage.&lt;/p&gt;&lt;p&gt;Het rustig gelegen, comfortabele appartement is voorzien van een open keuken met hoogglans witte panelen en een zwart stenen aanrechtblad, energiezuinige cv-combi, compleet isolatiepakket, luxe sanitair en een badkamer met een inloopdouche. &lt;/p&gt;&lt;p&gt;Details:&lt;/p&gt;&lt;p&gt;- 90m2&lt;br /&gt;- Nieuwbouw&lt;br /&gt;- volledig gestoffeerd&lt;br /&gt;- geen bemiddelingscourtage, slechts €300,- administratiekosten&lt;br /&gt;- mogelijkheid om parkeerplaats in parkeergarage te huren voor € 45,- per maand&lt;br /&gt;- servicekosten € 49,- per maand&lt;/p&gt;&lt;p&gt;Nieuwsgierig? Neem contact op met ons kantoor en maak vrijblijvend een afspraak voor een bezichtiging.&lt;/p&gt;'));
		
		$this->assertArrayHasKey('volume', $houses[0]);
		
		
		// test address
		
		$this->assertEquals($houses[0]['street'], 'Gildenlaan');
		$this->assertEquals($houses[0]['postal_code'], '7329EC');
		$this->assertEquals($houses[0]['city'], 'Apeldoorn');
		$this->assertEquals($houses[0]['country'], 'Nederland');
		$this->assertArrayHasKey('house_number', $houses[0]);
		
		
		// test type and build type
		$this->assertEquals($houses[0]['type'], 'Appartement');
		$this->assertEquals($houses[0]['build_type'], 'Unknown');
		
		
		// only small check to ensure second house is available
		
		$this->assertEquals($houses[1]['price'], '1150');
		$this->assertEquals($houses[1]['street'], 'Franciscanenstraat');
		
		// test attributes
		$this->assertNotContains('Garden', $houses[0]['attributes'], 'Pararius feed expected first house not to have a garden');
		$this->assertContains('Gestoffeerd', $houses[0]['attributes'], 'Pararius feed expected first house to have an attribute gestoffeerd');
		$this->assertContains('Garage', $houses[0]['attributes'], 'Pararius feed expected first house to have a garage');
		
		
		// test images
		$this->assertCount(11, $houses[0]['images'], "Pararius feed expected to find 11 images for first house...");
		$this->assertCount(27, $houses[1]['images'], "Pararius feed expected to find 27 images for second house...");
		
		$this->assertEquals('http://public.parariusoffice.nl/274/photos/export/42.1415045254-233.jpg', $houses[0]['images'][0]['url']);
		$this->assertEquals('http://public.parariusoffice.nl/274/photos/export/55.1420813116-500.jpg', $houses[1]['images'][0]['url']);
		
	}
}