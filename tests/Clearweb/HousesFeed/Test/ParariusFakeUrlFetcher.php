<?php namespace Clearweb\HousesFeed\Test;

use Clearweb\HousesFeed\Fetcher\IUrlFetcher;

class ParariusFakeUrlFetcher implements IUrlFetcher
{
	public function fetch($url)
	{
		return file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))).'/data/pararius_example.xml');
	}
}